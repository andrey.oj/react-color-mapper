import {
    addNewItem,
    checkForChainDeps,
    checkForCyclicDeps,
    checkForDomainDuplications,
    checkForRangeDuplications,
    editExistingItem
} from '../src/redux/logics/Validators';
import {
    INITIAL_CHAINED_ARRAY,
    INITIAL_CYCLIC_ARRAY,
    INITIAL_DUPLICATE_DOMAIN_ARRAY,
    INITIAL_DUPLICATE_RANGE_ARRAY,
    INITIAL_GOOD_ARRAY
} from '../src/StarterEntries';

describe('Bad input values', () => {
    let colorMap;

    beforeEach(() => {
        colorMap = INITIAL_GOOD_ARRAY.slice();
    });
    it('should fail because of missing colormap', () => {
        const result = addNewItem(null, 'Test', 'Best');
        expect(result.errors.colorMap.text).toEqual('Color map is empty');
    });
    it('should fail because of missing domain', () => {
        const result = addNewItem(colorMap, '', 'Black');
        expect(result.errors.domain.text).toEqual('Domain is empty');
    });
    it('should fail because of missing range', () => {
        const result = addNewItem(colorMap, 'Test', '');
        expect(result.errors.range.text).toEqual('Range is empty');
    });
});

describe('Cyclic dependencies', () => {
    let colorMap;

    beforeEach(() => {
        colorMap = INITIAL_CYCLIC_ARRAY.slice();
    });

    it('finds no cyclic dependencies in good list', () => {
        const result = checkForCyclicDeps(INITIAL_GOOD_ARRAY.slice());
        expect(result).toEqual({});
    });

    it('finds no cyclic dependencies in duplicate domain list', () => {
        const result = checkForCyclicDeps(INITIAL_DUPLICATE_DOMAIN_ARRAY.slice());
        expect(result).toEqual({});
    });

    it('finds no cyclic dependencies in duplicate range list', () => {
        const result = checkForCyclicDeps(INITIAL_DUPLICATE_RANGE_ARRAY.slice());
        expect(result).toEqual({});
    });

    it('finds cyclic dependencies', () => {
        const result = checkForCyclicDeps(colorMap);
        expect(result['0'].text).toEqual('Cyclic dependency: Stonegrey<->Dark Grey');
    });
});

describe('Chain dependencies', () => {
    let colorMap;

    beforeEach(() => {
        colorMap = INITIAL_CHAINED_ARRAY.slice();
    });

    it('finds no chain dependencies in good list', () => {
        const result = checkForChainDeps(INITIAL_GOOD_ARRAY.slice());
        expect(result).toEqual({});
    });

    it('finds no chain dependencies in duplicate domain list', () => {
        const result = checkForChainDeps(INITIAL_DUPLICATE_DOMAIN_ARRAY.slice());
        expect(result).toEqual({});
    });

    it('finds no chain dependencies in duplicate range list', () => {
        const result = checkForChainDeps(INITIAL_DUPLICATE_RANGE_ARRAY.slice());
        expect(result).toEqual({});
    });

    it('finds exactly 3 chained dependencies', () => {
        const result = checkForChainDeps(colorMap);
        expect(Object.keys(result).length).toEqual(3);
    });

    it('finds chained dependencies', () => {
        const result = checkForChainDeps(colorMap);
        expect(result['1'].text).toEqual('Chained dependency: Silver->Black');
        expect(result['2'].text).toEqual('Chained dependency: Blue->Silver');
        expect(result['3'].text).toEqual('Chained dependency: Black->Blue');
    });
});

describe('Domain Duplication dependencies', () => {
    let colorMap;

    beforeEach(() => {
        colorMap = INITIAL_DUPLICATE_DOMAIN_ARRAY.slice();
    });

    it('finds exactly 2 duplications', () => {
        const result = checkForDomainDuplications(colorMap);
        expect(Object.keys(result).length).toEqual(2);
    });

    it('finds duplications', () => {
        const result = checkForDomainDuplications(colorMap);
        expect(result['0'].text).toEqual('Domain duplication found: Stonegrey');
        expect(result['1'].text).toEqual('Domain duplication found: Stonegrey');
    });
});

describe('Range Duplication dependencies', () => {
    let colorMap;

    beforeEach(() => {
        colorMap = INITIAL_DUPLICATE_RANGE_ARRAY.slice();
    });

    it('finds exactly 2 duplications', () => {
        const result = checkForRangeDuplications(colorMap);
        expect(Object.keys(result).length).toEqual(2);
    });

    it('finds duplications', () => {
        const result = checkForRangeDuplications(colorMap);
        expect(result['0'].text).toEqual('Range duplication found: Black');
        expect(result['1'].text).toEqual('Range duplication found: Black');
    });
});

describe('Adding Rows', () => {
    let colorMap;

    beforeEach(() => {
        colorMap = INITIAL_GOOD_ARRAY.slice();
    });
    it('should add the value', () => {
        const result = addNewItem(colorMap, 'Test', 'Best');
        expect(result.errors).toEqual({});
        expect(result.colorMap[`${colorMap.length - 1}`].range).toEqual('Best');
    });

    it('should add the value: Existing domain', () => {
        const result = addNewItem(colorMap, 'Stonegrey', 'Best');
        expect(result.colorMap[`${colorMap.length - 1}`].range).toEqual('Best');
    });
    it('should not add the value: Existing range', () => {
        const result = addNewItem(colorMap, 'StonegreyX', 'Dark Grey');
        expect(result.colorMap[`${colorMap.length - 1}`].range).toEqual('Dark Grey');
    });

    it('adds entry and shows error: Cyclic', () => {
        const result = addNewItem(colorMap, 'Dark Grey', 'Stonegrey');
        expect(result.errors[`${colorMap.length - 1}`].text).toEqual(
            'Cyclic dependency: Dark Grey<->Stonegrey'
        );
        expect(result.colorMap[`${colorMap.length - 1}`].range).toEqual('Stonegrey');
    });

    it('adds entry and shows error: Chain', () => {
        const result = addNewItem(colorMap, 'Black', 'White');
        expect(result.errors['1'].text).toEqual('Chained dependency: Midnight Black->Black');
        expect(result.colorMap[`${colorMap.length - 1}`].range).toEqual('White');
    });
});

describe('Editing Rows', () => {
    let colorMap;

    beforeEach(() => {
        colorMap = INITIAL_GOOD_ARRAY.slice();
    });

    it('should edit the row: Range', () => {
        const result = editExistingItem(colorMap, 'Stonegrey', 'Dark Grey X', '0');
        expect(result.errors).toEqual({});
    });

    it('fails editing because duplicate range', () => {
        const result = editExistingItem(colorMap, 'Stonegrey', 'Blue', '0');
        expect(result.errors['0'].text).toEqual('Range duplication found: Blue');
    });
});

const INITIAL_SWAPPED_CYCLIC_ARRAY = [
    {
        domain: 'Dark Grey',
        range: 'Stonegrey',
        key: 0
    },
    {
        domain: 'Stonegrey',
        range: 'Dark Grey',
        key: 1
    },
    {
        domain: 'Mystic Silver',
        range: 'Silver',
        key: 2
    },
    {
        domain: 'Anthracite',
        range: 'Blue',
        key: 3
    }
];

const INITIAL_SWAPPED_CHAINED_ARRAY = [
    {
        domain: 'Stonegrey',
        range: 'Grey',
        key: 0
    },
    {
        domain: 'Blue',
        range: 'Silver',
        key: 1
    },
    {
        domain: 'Black',
        range: 'Blue',
        key: 2
    },
    {
        domain: 'Silver',
        range: 'Black',
        key: 3
    }
];

const INITIAL_SWAPPED_CHAINED_ARRAY2 = [
    {
        domain: 'Stonegrey',
        range: 'Dark Grey1',
        key: 1
    },

    {
        domain: 'Dark Grey',
        range: 'Stonegrey',
        key: 0
    },

    {
        domain: 'Mystic Silver',
        range: 'Silver',
        key: 2
    },
    {
        domain: 'Anthracite',
        range: 'Blue',
        key: 3
    }
];

describe('Advanced Cyclic dependencies', () => {
    it('finds cyclic dependencies in swapped array', () => {
        const result = checkForCyclicDeps(INITIAL_SWAPPED_CYCLIC_ARRAY);
        expect(result['0'].text).toEqual('Cyclic dependency: Dark Grey<->Stonegrey');
    });
});

describe('Advanced Chain dependencies', () => {
    it('finds chained dependencies in swapped array', () => {
        const result = checkForChainDeps(INITIAL_SWAPPED_CHAINED_ARRAY);
        expect(result['3'].text).toEqual('Chained dependency: Silver->Black');
        expect(result['1'].text).toEqual('Chained dependency: Blue->Silver');
        expect(result['2'].text).toEqual('Chained dependency: Black->Blue');
    });

    it('finds chain dependencies in swapped array', () => {
        const result = checkForChainDeps(INITIAL_SWAPPED_CHAINED_ARRAY2);
        expect(result['0'].text).toEqual('Chained dependency: Dark Grey->Stonegrey');
    });
});
