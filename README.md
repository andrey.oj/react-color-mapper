# README

### Run
**Before running the project, please clear you localStorage if you used previous versions**

Install the node modules, use

**npm install** or **yarn**

To start the project use

**npm start** or **yarn start**

### "Database"

This is a server-less application

Everything is saved in **localStorage**, there are some default lists created when you run the project

To reset it in Chrome: Chrome>Application>Storage>clear local storage

### Testing

To run the tests, use

**npm test** or **yarn test**

!!I had to use not the newest jest, because it conflicts with the create-react-app config, my version is 21.2.0

I only tested the most needed part: the table validation

No Redux or Click tests

### Notes

I used **ant design** https://ant.design/

I did not use **Webpack** because of lack of time 

This project was created with **create-react-app**

This app was only tested in chrome browser, Edge and IE might have some CSS issues with ant-design



Created by Andrey Paramonov 03.10.2018