import React, { Component } from 'react';
import { Provider } from 'react-redux';
import 'antd/dist/antd.css';
import configureStore from './redux/configureStore';
import MainView from './components/MainView';
import { initDictionaries } from './redux/actions/dictionaryActions';

const store = configureStore();

class App extends Component {
    constructor() {
        super();
        store.dispatch(initDictionaries());
    }

    render() {
        return (
            <Provider store={store}>
                <MainView />
            </Provider>
        );
    }
}

export default App;
