const DICTIONARIES = [
    'Good dictionary',
    'Duplicate ranges',
    'Duplicate domains',
    'Cyclic dictionary',
    'Chained dictionary',
    'Catastrophic dictionary'
];
const INITIAL_GOOD_ARRAY = [
    {
        domain: 'Stonegrey',
        range: 'Dark Grey',
        key: 0
    },
    {
        domain: 'Midnight Black',
        range: 'Black',
        key: 1
    },
    {
        domain: 'Mystic Silver',
        range: 'Silver',
        key: 2
    },
    {
        domain: 'Anthracite',
        range: 'Blue',
        key: 3
    }
];
const INITIAL_DUPLICATE_RANGE_ARRAY = [
    {
        domain: 'Stonegrey',
        range: 'Black',
        key: 0
    },
    {
        domain: 'Midnight Black',
        range: 'Black',
        key: 1
    },
    {
        domain: 'Mystic Silver',
        range: 'Silver',
        key: 2
    },
    {
        domain: 'Anthracite',
        range: 'Blue',
        key: 3
    }
];
const INITIAL_DUPLICATE_DOMAIN_ARRAY = [
    {
        domain: 'Stonegrey',
        range: 'Grey',
        key: 0
    },
    {
        domain: 'Stonegrey',
        range: 'Black',
        key: 1
    },
    {
        domain: 'Mystic Silver',
        range: 'Silver',
        key: 2
    },
    {
        domain: 'Anthracite',
        range: 'Blue',
        key: 3
    }
];
const INITIAL_CHAINED_ARRAY = [
    {
        domain: 'Stonegrey',
        range: 'Grey',
        key: 0
    },
    {
        domain: 'Silver',
        range: 'Black',
        key: 1
    },
    {
        domain: 'Blue',
        range: 'Silver',
        key: 2
    },
    {
        domain: 'Black',
        range: 'Blue',
        key: 3
    }
];

const INITIAL_CYCLIC_ARRAY = [
    {
        domain: 'Stonegrey',
        range: 'Dark Grey',
        key: 0
    },
    {
        domain: 'Dark Grey',
        range: 'Stonegrey',
        key: 1
    },
    {
        domain: 'Mystic Silver',
        range: 'Silver',
        key: 2
    },
    {
        domain: 'Anthracite',
        range: 'Blue',
        key: 3
    }
];

const INITIAL_FULL_OF_ERRORS_ARRAY = [
    {
        domain: 'Stonegrey',
        range: 'Dark Grey',
        key: 0
    },
    {
        domain: 'Dark Grey',
        range: 'Stonegrey',
        key: 1
    },
    {
        domain: 'Mystic Silver',
        range: 'Silver',
        key: 2
    },
    {
        domain: 'Anthracite',
        range: 'Blue',
        key: 3
    },
    {
        domain: 'Mystic Grey',
        range: 'Silver',
        key: 4
    },
    {
        domain: 'Mystic Grey',
        range: 'Red',
        key: 5
    },
    {
        domain: 'Lemon',
        range: 'Yellow',
        key: 6
    },
    {
        domain: 'Lime',
        range: 'Yellow',
        key: 6
    },
    {
        domain: 'Yellow',
        range: 'Mellow',
        key: 7
    },
    {
        domain: 'Mellow',
        range: 'Blue',
        key: 7
    }
];

const DICTIONARY_MAPPING = {
    [DICTIONARIES[0]]: INITIAL_GOOD_ARRAY,
    [DICTIONARIES[1]]: INITIAL_DUPLICATE_DOMAIN_ARRAY,
    [DICTIONARIES[2]]: INITIAL_DUPLICATE_RANGE_ARRAY,
    [DICTIONARIES[3]]: INITIAL_CYCLIC_ARRAY,
    [DICTIONARIES[4]]: INITIAL_CHAINED_ARRAY,
    [DICTIONARIES[5]]: INITIAL_FULL_OF_ERRORS_ARRAY
};

export {
    INITIAL_GOOD_ARRAY,
    INITIAL_DUPLICATE_DOMAIN_ARRAY,
    INITIAL_DUPLICATE_RANGE_ARRAY,
    INITIAL_CYCLIC_ARRAY,
    INITIAL_CHAINED_ARRAY,
    DICTIONARIES,
    DICTIONARY_MAPPING
};
