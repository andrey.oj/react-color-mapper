import { combineReducers } from 'redux';

import dictionaries from './reducers/dictionaryReducer';
import maps from './reducers/mapsReducer';

const rootReducer = combineReducers({
    maps,
    dictionaries
});

export default rootReducer;
