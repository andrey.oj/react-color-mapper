import * as Types from '../actionTypes';

export default function dictionaryReducer(
    state = {
        current: '',
        dictionaries: [],
        isCreating: false,
        createName: '',
        nameIsConflicting: false
    },
    action
) {
    switch (action.type) {
        case Types.START_CREATING_NEW_DICTIONARY:
            return {
                ...state,
                isCreating: true,
                createName: ''
            };
        case Types.EDIT_CREATING_DICTIONARY_NAME:
            return {
                ...state,
                createName: action.payload,
                nameIsConflicting: state.dictionaries.indexOf(action.payload) >= 0
            };
        case Types.DONE_CREATING_NEW_DICTIONARY:
            return {
                ...state,
                isCreating: false
            };
        case Types.UPDATE_DICTIONARIES:
            return {
                ...state,
                dictionaries: action.payload
            };
        case Types.SET_DICTIONARY:
            return {
                ...state,
                current: action.payload,
                isCreating: false
            };

        default:
            return state;
    }
}
