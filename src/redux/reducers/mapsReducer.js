import * as Types from '../actionTypes';
import { KEY_NEW } from '../actions/mapsActions';

const DEFAULT_PRODUCT_ENTITIES = {
    domain: '',
    range: '',
    key: KEY_NEW
};

export default function storeReducer(
    state = {
        colorMapping: [],
        errors: {},
        isCreatingItem: null,
        isEditingItem: null
    },
    action
) {
    switch (action.type) {
        case Types.START_CREATING_STORE_ITEM:
            return {
                ...state,
                isCreatingItem: DEFAULT_PRODUCT_ENTITIES
            };
        case Types.START_EDITING_STORE_ITEM:
            return {
                ...state,
                isEditingItem: {
                    ...action.payload
                }
            };
        case Types.EDIT_CREATING_ITEM:
            return {
                ...state,
                isCreatingItem: {
                    ...state.isCreatingItem,
                    [action.payload.entity]: action.payload.text
                }
            };
        case Types.EDIT_EXISTING_ITEM:
            return {
                ...state,
                isEditingItem: {
                    ...state.isEditingItem,
                    [action.payload.entity]: action.payload.text
                }
            };
        case Types.UPDATE_COLORMAP:
            return {
                ...state,
                isEditingItem: null,
                isCreatingItem: null,
                colorMapping: action.payload
            };

        case Types.CANCEL_EDITING_ITEM:
            return {
                ...state,
                isEditingItem: null,
                isCreatingItem: null
            };
        case Types.SET_ERRORS:
            return {
                ...state,
                errors: action.payload
            };
        default:
            return state;
    }
}
