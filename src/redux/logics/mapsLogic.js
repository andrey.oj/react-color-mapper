import { createLogic } from 'redux-logic';
import {
    backupStore,
    loadColorMap,
    loadColorMapDone,
    setErrors,
    updateColorMap
} from '../actions/mapsActions';
import { showError, showNotification } from '../actions/notificationActions';
import { addNewItem, editExistingItem, validateMap } from './Validators';
import { DICTIONARY_MAPPING } from '../../StarterEntries';
import * as Types from '../actionTypes';

/**
 * Loads dictionary from memory or inits new Example array
 * */
const loadMapLogic = createLogic({
    type: Types.LOAD_COLOR_MAP,
    latest: false,

    process({ action }, dispatch, done) {
        /* Assume we get our stuff from database */
        const currentDictionary = action.payload;
        const storeStringData = localStorage.getItem(`dictionary-${currentDictionary}`);

        let data;
        if (!storeStringData) {
            // Store initial values
            data = DICTIONARY_MAPPING[currentDictionary];
            if (!data) data = [];
            dispatch(backupStore(data));
        } else {
            data = JSON.parse(storeStringData);
        }
        // Avoid nullpointer with older versions
        if (!data.length) data = [];

        dispatch(loadColorMapDone(data));

        done();
    }
});
/**
 * Puts dictionary into redux store and checks for errors
 * */
const loadColorMapDoneLogic = createLogic({
    type: Types.LOAD_COLOR_MAP_DONE,
    latest: false,

    process({ action }, dispatch, done) {
        const colorMapping = action.payload;
        dispatch(updateColorMap(colorMapping));
        const errors = validateMap(colorMapping);
        dispatch(setErrors(errors));

        done();
    }
});
/**
 * Re-loads current dictionary
 * */
const cancelEditingItemLogic = createLogic({
    type: Types.CANCEL_EDITING_ITEM,
    latest: false,

    process({ getState }, dispatch, done) {
        const { current } = getState().dictionaries;
        dispatch(loadColorMap(current));

        done();
    }
});

/**
 * Removes selected row and dispatches save into db
 * */
const deleteItemLogic = createLogic({
    type: Types.DELETE_SELECTED_ITEM,
    latest: false,

    process({ getState }, dispatch, done) {
        const { isEditingItem, colorMapping } = getState().maps;
        const newMap = colorMapping.filter(x => x.key !== isEditingItem.key);
        dispatch(backupStore(newMap));
        dispatch(loadColorMapDone(newMap));
        done();
    }
});

/**
 * Saves dictionary into "db"
 * */
const backupStoreLogic = createLogic({
    type: Types.BACKUP_STORE,
    latest: false,

    process({ action, getState }, dispatch, done) {
        const { current } = getState().dictionaries;
        /* Assume we store this in a database */
        const data = action.payload;
        localStorage.setItem(`dictionary-${current}`, JSON.stringify(data));

        done();
    }
});

/**
 * Is called after adding or editing row
 * */
const processEntry = (errors, colorMap, dispatch, done) => {
    const errorTypes = Object.keys(errors);
    if (errorTypes.length) {
        const error = errors[errorTypes[0]];
        dispatch(showNotification(error.level, error.text));
    }
    dispatch(setErrors(errors));
    dispatch(updateColorMap(colorMap));
    dispatch(backupStore(colorMap));

    done();
};

/**
 * Called to create new entry
 * */
const createNewEntry = createLogic({
    type: Types.CREATE_NEW_STORE_ENTRY,
    latest: false,

    process({ getState }, dispatch, done) {
        const { colorMapping } = getState().maps;
        const { domain, range } = getState().maps.isCreatingItem;
        const { errors, creationErrors, colorMap } = addNewItem(colorMapping, domain, range);
        // If problems with creation, do not process
        const errorKeys = Object.keys(creationErrors);
        if (errorKeys.length) {
            dispatch(showError(creationErrors[errorKeys[0]].text));
        } else {
            processEntry(errors, colorMap, dispatch, done);
        }
    }
});

/**
 * Called to edit current entry
 * */
const editEntry = createLogic({
    type: Types.DONE_EDITING_ITEM,
    latest: true,

    process({ getState }, dispatch, done) {
        const { colorMapping } = getState().maps;
        const { domain, range, key } = getState().maps.isEditingItem;
        const { errors, colorMap } = editExistingItem(colorMapping, domain, range, key);
        processEntry(errors, colorMap, dispatch, done);
    }
});

export default [
    loadMapLogic,
    backupStoreLogic,
    createNewEntry,
    editEntry,
    cancelEditingItemLogic,
    deleteItemLogic,
    loadColorMapDoneLogic
];
