import { createLogic } from 'redux-logic';
import {
    setCurrentDictionary,
    updateDictionaries,
    backupDictionaries
} from '../actions/dictionaryActions';
import { loadColorMap } from '../actions/mapsActions';
import { showNotification } from '../actions/notificationActions';
import { DICTIONARIES } from '../../StarterEntries';
import * as Types from '../actionTypes';

const initDictionariesLogic = createLogic({
    type: Types.INIT_DICTIONARIES,
    latest: false,

    process(ignore, dispatch, done) {
        /* Assume we get our stuff from database */
        const storeStringData = localStorage.getItem('customer-dictionaries-data');

        let data;
        if (!storeStringData) {
            // Store initial values
            data = DICTIONARIES;
            dispatch(backupDictionaries(data));
        } else {
            data = JSON.parse(storeStringData);
        }

        const currentDictionary = data[0];

        dispatch(updateDictionaries(data));

        dispatch(setCurrentDictionary(currentDictionary));

        done();
    }
});

const doneCreatingDict = createLogic({
    type: Types.DONE_CREATING_NEW_DICTIONARY,
    latest: false,

    process({ action, getState }, dispatch, done) {
        const { dictionaries } = getState().dictionaries;
        const name = action.payload;
        if (name) {
            if (dictionaries.indexOf(name) >= 0) {
                dispatch(
                    showNotification(
                        'error',
                        'There is already an existing dictionary with this name'
                    )
                );
            } else {
                dictionaries.push(name);
                dispatch(backupDictionaries(dictionaries));
                dispatch(updateDictionaries(dictionaries));
                dispatch(setCurrentDictionary(name));
            }
        }
        done();
    }
});

const deleteDictLogic = createLogic({
    type: Types.DELETE_CURRENT_DICTIONARY,
    latest: false,

    process({ getState }, dispatch, done) {
        const { dictionaries, current } = getState().dictionaries;

        dictionaries.splice(dictionaries.indexOf(current), 1);
        dispatch(backupDictionaries(dictionaries));
        dispatch(updateDictionaries(dictionaries));
        if (dictionaries.length) {
            dispatch(setCurrentDictionary(dictionaries[0]));
        } else {
            dispatch(setCurrentDictionary(''));
        }

        done();
    }
});

const setDictionaryLogic = createLogic({
    type: Types.SET_DICTIONARY,
    latest: false,

    process({ action }, dispatch, done) {
        const name = action.payload;
        dispatch(loadColorMap(name));
        done();
    }
});

const backupStoreLogic = createLogic({
    type: Types.BACKUP_DICTIONARIES,
    latest: false,

    process({ action }, dispatch, done) {
        /* Assume we get our stuff from database */
        const data = action.payload;
        localStorage.setItem('customer-dictionaries-data', JSON.stringify(data));

        done();
    }
});

const createNewEntry = createLogic({
    type: Types.CREATE_NEW_DICTIONARY_ENTRY,
    latest: false,

    process({ getState, action }, dispatch, done) {
        const { dictionaries } = getState().dictionaries;
        const name = action.payload;

        if (dictionaries.indexOf(name) >= 0) {
            dispatch(showNotification('error', 'Dictionary already exists'));
        } else {
            dictionaries.push(name);
            dispatch(updateDictionaries(dictionaries));
            dispatch(setCurrentDictionary(name));
        }

        done();
    }
});

export default [
    initDictionariesLogic,
    backupStoreLogic,
    createNewEntry,
    setDictionaryLogic,
    doneCreatingDict,
    deleteDictLogic
];
