const ERRORS = {
    ColorIsEmpty: {
        text: 'Color map is empty',
        level: 'warn'
    },
    RangeIsEmpty: {
        text: 'Range is empty',
        level: 'warn'
    },
    DomainIsEmpty: {
        text: 'Domain is empty',
        level: 'warn'
    },
    DomainDuplication: domain => ({
        text: `Domain duplication found: ${domain}`,
        level: 'warn'
    }),
    RangeDuplication: range => ({
        text: `Range duplication found: ${range}`,
        level: 'warn'
    }),
    CyclicDependency: arr => ({
        text: `Cyclic dependency: ${arr.join('<->')}`,
        level: 'error'
    }),
    ChainDependency: arr => ({
        text: `Chained dependency: ${arr.join('->')}`,
        level: 'error'
    })
};

function validateNotEmptyInput(colorMap, domain, range) {
    if (!colorMap) return { colorMap: ERRORS.ColorIsEmpty };
    if (!domain) return { domain: ERRORS.DomainIsEmpty };
    if (!range) return { range: ERRORS.RangeIsEmpty };
    return null;
}

function generateKey(colorMap) {
    return colorMap.length ? Math.max.apply(null, colorMap.map(x => +x.key)) + 1 : 0;
}

function hasDomain(colorMap, domain, fromIndex = 0) {
    const domains = colorMap.filter((entry, i) => i > fromIndex - 1 && entry.domain === domain);

    return domains.length > 0 ? domains : null;
}

function hasRange(colorMap, range, fromIndex = 0) {
    const ranges = colorMap.filter((entry, i) => i > fromIndex && entry.range === range);
    return ranges.length > 0 ? ranges : null;
}

function checkForCyclicDeps(colorMap) {
    const errors = {};

    colorMap.forEach(entry => {
        const { domain, range, key } = entry;

        const domainSameRange = hasDomain(colorMap, range);
        if (domainSameRange) {
            domainSameRange.forEach(e => {
                if (e.range === domain) {
                    errors[key] = ERRORS.CyclicDependency([domain, e.domain]);
                    errors[e.key] = ERRORS.CyclicDependency([e.domain, domain]);
                }
            });
        }
    });

    return errors;
}

function checkForChainDeps(colorMap) {
    const errors = {};
    colorMap.forEach(entry => {
        const { domain, range, key } = entry;

        const domainSameRange = hasDomain(colorMap, range);
        if (domainSameRange) {
            const [sameDoms] = domainSameRange;
            errors[key] = ERRORS.ChainDependency([domain, sameDoms.domain]);
        }
    });

    return errors;
}

function checkForDomainDuplications(colorMap) {
    const errors = {};
    colorMap.forEach((entry, i) => {
        const { domain, key } = entry;

        const sameDomain = hasDomain(colorMap, domain, i + 1);
        if (sameDomain) {
            sameDomain.forEach(e => {
                errors[e.key] = ERRORS.DomainDuplication(domain);
            });
            errors[key] = ERRORS.DomainDuplication(domain);
        }
    });

    return errors;
}

function checkForRangeDuplications(colorMap) {
    const errors = {};
    colorMap.forEach((entry, i) => {
        const { range, key } = entry;

        const sameRange = hasRange(colorMap, range, i);
        if (sameRange) {
            sameRange.forEach(e => {
                errors[e.key] = ERRORS.RangeDuplication(range);
            });
            errors[key] = ERRORS.RangeDuplication(range);
        }
    });

    return errors;
}

function validateMap(colorMap, overrideCyclic = true) {
    if (!colorMap) return { colorMap: ERRORS.ColorIsEmpty };

    const cyclicDeps = checkForCyclicDeps(colorMap);

    // Need to check if already a cyclic dep exists, because chained will override cyclic
    const chainDeps =
        !Object.keys(cyclicDeps).length || overrideCyclic ? checkForChainDeps(colorMap) : {};

    const rangeDuplications = checkForRangeDuplications(colorMap);

    const domainDuplications = checkForDomainDuplications(colorMap);

    // Add by severity, more severe override less severe
    return {
        ...rangeDuplications,
        ...domainDuplications,
        ...chainDeps,
        ...cyclicDeps
    };
}

function addNewItem(colorMap, domain, range) {
    let errors = validateNotEmptyInput(colorMap, domain, range);
    if (errors) return { errors, colorMap };
    const creationErrors = {};
    colorMap.push({ domain, range, key: generateKey(colorMap) });

    errors = validateMap(colorMap, false);

    return {
        creationErrors,
        errors,
        colorMap
    };
}

function editExistingItem(colorMap, domain, range, key) {
    let errors = validateNotEmptyInput(colorMap, domain, range);
    if (errors) return { errors, colorMap };

    for (let i = 0; i < colorMap.length; i += 1) {
        if (+colorMap[i].key === +key) {
            colorMap[i] = { range, domain, key };
            break;
        }
    }
    errors = validateMap(colorMap);

    return {
        errors,
        colorMap
    };
}

export {
    editExistingItem,
    addNewItem,
    checkForDomainDuplications,
    checkForRangeDuplications,
    checkForChainDeps,
    checkForCyclicDeps,
    validateMap
};
