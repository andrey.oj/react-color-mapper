import { createLogic } from 'redux-logic';
import { message } from 'antd';
import * as Types from '../actionTypes';

const showNotificationLogic = createLogic({
    type: Types.SHOW_NOTIFICATION,
    latest: false,

    process({ action }, dispatch, done) {
        message[action.payload.type](action.payload.text);
        done();
    }
});

export default [showNotificationLogic];
