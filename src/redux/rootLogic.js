import mapsLogic from './logics/mapsLogic';
import dictionaryLogic from './logics/dictionaryLogic';
import notificationLogic from './logics/notificationLogic';

export default [...mapsLogic, ...dictionaryLogic, ...notificationLogic];
