import { applyMiddleware, createStore } from 'redux';
import { createLogicMiddleware } from 'redux-logic';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootLogic from './rootLogic';
import rootReducer from './rootReducer';

const deps = {
    //  No dependencies
};

const logicMiddleware = createLogicMiddleware(rootLogic, deps);

export const configureStore = () =>
    createStore(rootReducer, composeWithDevTools({})(applyMiddleware(logicMiddleware)));

export default configureStore;
