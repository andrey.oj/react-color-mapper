import * as Types from '../actionTypes';

const initDictionaries = () => ({
    type: Types.INIT_DICTIONARIES
});
const editCreatingDictionaryName = name => ({
    type: Types.EDIT_CREATING_DICTIONARY_NAME,
    payload: name
});
const setCurrentDictionary = name => ({
    type: Types.SET_DICTIONARY,
    payload: name
});
const updateDictionaries = dictionaries => ({
    type: Types.UPDATE_DICTIONARIES,
    payload: dictionaries
});
const backupDictionaries = dictionaries => ({
    type: Types.BACKUP_DICTIONARIES,
    payload: dictionaries
});

const startCreatingNewDictionary = () => ({
    type: Types.START_CREATING_NEW_DICTIONARY
});
const doneCreatingNewDictionary = name => ({
    type: Types.DONE_CREATING_NEW_DICTIONARY,
    payload: name
});
const deleteCurrentDictionary = () => ({
    type: Types.DELETE_CURRENT_DICTIONARY
});

export {
    initDictionaries,
    setCurrentDictionary,
    backupDictionaries,
    updateDictionaries,
    startCreatingNewDictionary,
    doneCreatingNewDictionary,
    deleteCurrentDictionary,
    editCreatingDictionaryName
};
