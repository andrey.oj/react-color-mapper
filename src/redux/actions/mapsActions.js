import * as Types from '../actionTypes';
const KEY_NEW = -1;

const loadColorMap = dictionary => ({
    type: Types.LOAD_COLOR_MAP,
    payload: dictionary
});

const loadColorMapDone = data => ({
    type: Types.LOAD_COLOR_MAP_DONE,
    payload: data
});
const setErrors = errors => ({
    type: Types.SET_ERRORS,
    payload: errors
});

const updateColorMap = colorMap => ({
    type: Types.UPDATE_COLORMAP,
    payload: colorMap
});

const backupStore = data => ({
    type: Types.BACKUP_STORE,
    payload: data
});

const startCreatingItem = () => ({
    type: Types.START_CREATING_STORE_ITEM
});

const startEditingItem = (domain, range, key) => ({
    type: Types.START_EDITING_STORE_ITEM,
    payload: { domain, range, key }
});

const editExistingItem = (entity, text) => ({
    type: Types.EDIT_EXISTING_ITEM,
    payload: { entity, text }
});
const editCreatingItem = (entity, text) => ({
    type: Types.EDIT_CREATING_ITEM,
    payload: { entity, text }
});

const createNewItem = () => ({
    type: Types.CREATE_NEW_STORE_ENTRY
});

const doneEditingItem = () => ({
    type: Types.DONE_EDITING_ITEM
});

const cancelEditItem = () => ({
    type: Types.CANCEL_EDITING_ITEM
});
const deleteSelectedItem = () => ({
    type: Types.DELETE_SELECTED_ITEM
});
export {
    loadColorMap,
    loadColorMapDone,
    backupStore,
    createNewItem,
    startCreatingItem,
    startEditingItem,
    editCreatingItem,
    editExistingItem,
    doneEditingItem,
    updateColorMap,
    cancelEditItem,
    deleteSelectedItem,
    setErrors,
    KEY_NEW
};
