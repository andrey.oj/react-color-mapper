import * as Types from '../actionTypes';

const showNotification = (type, text) => ({
    type: Types.SHOW_NOTIFICATION,
    payload: { type, text }
});
const showError = text => ({
    type: Types.SHOW_NOTIFICATION,
    payload: { type: 'error', text }
});

export { showNotification, showError };
