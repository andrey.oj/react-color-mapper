import React from 'react';
import styled from 'styled-components';
import { Row } from 'antd';

import ColorMapTable from './ColorMapTable';
import DictionarySelector from './DictionarySelector';

const StyledView = styled.div`
    padding: 20px 40px 20px 40px;
    max-width: 800px;
    margin-left: auto;
    margin-right: auto;
`;
const MainView = () => (
    <StyledView>
        <Row>
            <DictionarySelector />
        </Row>
        <br />
        <Row>
            <ColorMapTable />
        </Row>
    </StyledView>
);

export default MainView;
