import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Select, Popconfirm, Tooltip, Card, Input, Col, Row, Button } from 'antd';
import {
    startCreatingNewDictionary,
    doneCreatingNewDictionary,
    setCurrentDictionary,
    deleteCurrentDictionary,
    editCreatingDictionaryName
} from '../redux/actions/dictionaryActions';

const { Option } = Select;

class DictionarySelector extends Component {
    submitNewDictionary = () => {
        this.props.doneCreatingNewDictionary(this.props.createName);
    };

    cancelNewDictionary = () => {
        this.props.doneCreatingNewDictionary('');
    };

    startCreateDictionary = () => {
        this.props.startCreatingNewDictionary();
    };

    deleteDictionary = () => {
        this.props.deleteCurrentDictionary();
    };

    handleNameInput = e => {
        this.props.editCreatingDictionaryName(e.target.value);
    };

    handleSelectDictionary = name => {
        this.props.setCurrentDictionary(name);
    };

    renderConfirmSaveButton = (isCreating, nameIsConflicting) => {
        if (!isCreating) return null;

        const button = (
            <Button
                disabled={nameIsConflicting}
                icon="save"
                type="primary"
                onClick={this.submitNewDictionary}
            >
                Save
            </Button>
        );
        if (nameIsConflicting) {
            return <Tooltip title="Dictionary already exists">{button}</Tooltip>;
        }
        return button;
    };

    renderButtons = (isCreating, nameIsConflicting, isCreatingRow) => (
        <Button.Group>
            {isCreating ? null : (
                <Button
                    disabled={isCreatingRow}
                    icon="plus"
                    type="primary"
                    onClick={this.startCreateDictionary}
                >
                    New dictionary
                </Button>
            )}
            {isCreating ? null : (
                <Popconfirm
                    title="Are you sure you want to delete this dictionary?"
                    onConfirm={this.deleteDictionary}
                    okText="Yes"
                    cancelText="Cancel"
                >
                    <Button disabled={isCreatingRow} icon="delete" type="danger" />
                </Popconfirm>
            )}
            {this.renderConfirmSaveButton(isCreating, nameIsConflicting)}
            {!isCreating ? null : (
                <Button icon="stop" onClick={this.cancelNewDictionary}>
                    Cancel
                </Button>
            )}
        </Button.Group>
    );

    render() {
        const {
            dictionaries,
            selectedDictionary,
            isCreating,
            createName,
            nameIsConflicting,
            isCreatingRow
        } = this.props;

        return (
            <Card
                title="Dictionaries"
                extra={this.renderButtons(isCreating, nameIsConflicting, isCreatingRow)}
            >
                <Row gutter={8}>
                    <Col span={24}>
                        {isCreating ? (
                            <Input
                                value={createName}
                                placeholder="Type a new name"
                                onChange={this.handleNameInput}
                            />
                        ) : (
                            <Select
                                size="large"
                                value={selectedDictionary}
                                placeholder="Select dictionary"
                                onChange={this.handleSelectDictionary}
                                style={{ width: '100%' }}
                            >
                                {dictionaries.map(d => (
                                    <Option value={d} key={d}>
                                        {d}
                                    </Option>
                                ))}
                            </Select>
                        )}
                    </Col>
                </Row>
            </Card>
        );
    }
}

const mapStateToProps = state => ({
    selectedDictionary: state.dictionaries.current,
    dictionaries: state.dictionaries.dictionaries,
    isCreating: state.dictionaries.isCreating,
    createName: state.dictionaries.createName,
    nameIsConflicting: state.dictionaries.nameIsConflicting,
    isCreatingRow: state.maps.isCreatingItem
});
const mapDispatchToProps = dispatch => ({
    startCreatingNewDictionary: () => dispatch(startCreatingNewDictionary()),
    doneCreatingNewDictionary: name => dispatch(doneCreatingNewDictionary(name)),
    setCurrentDictionary: name => dispatch(setCurrentDictionary(name)),
    deleteCurrentDictionary: () => dispatch(deleteCurrentDictionary()),
    editCreatingDictionaryName: name => dispatch(editCreatingDictionaryName(name))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DictionarySelector);
