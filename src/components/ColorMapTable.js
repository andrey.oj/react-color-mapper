import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Card, Table, Icon, Input, Tooltip } from 'antd';

import {
    startCreatingItem,
    editCreatingItem,
    createNewItem,
    startEditingItem,
    editExistingItem,
    doneEditingItem,
    cancelEditItem,
    deleteSelectedItem,
    KEY_NEW
} from '../redux/actions/mapsActions';

class ProductTable extends Component {
    getProductColumns = () => [
        {
            title: 'Domain',
            key: 'domain',
            dataIndex: 'domain',
            render: (text, record) => this.renderInput(text, record, 'domain', true)
        },
        {
            title: 'Range',
            key: 'range',
            dataIndex: 'range',
            render: (text, record) => this.renderInput(text, record, 'range')
        },
        {
            title: 'Valid',
            key: 'error',
            dataIndex: 'error',
            render: this.renderValidIcon
        },
        {
            title: 'Edit',
            key: 'editButton',
            dataIndex: 'editButton',
            render: this.renderEditButton
        }
    ];

    getErrorIcon = level => {
        switch (level) {
            case 'warn':
                return <Icon style={{ color: 'orange' }} type="warning" />;
            case 'error':
                return <Icon style={{ color: 'red' }} type="warning" />;
            default:
                return null;
        }
    };

    renderValidIcon = (text, record) => {
        const { error } = record;
        if (error) {
            return <Tooltip title={error.text}>{this.getErrorIcon(error.level)}</Tooltip>;
        }
        return <Icon style={{ color: 'green' }} type="check" />;
    };

    renderEditButton = (text, record) => {
        const { key } = this.props.isEditingItem ? this.props.isEditingItem : {};
        const creating = record.key === KEY_NEW;
        const editing = creating || key === record.key;
        if (editing) {
            return (
                <Button.Group>
                    <Button type="primary" onClick={() => this.doneEdit(record)} icon="check" />
                    <Button onClick={this.cancelEdit} icon="stop" />
                    {creating ? null : (
                        <Button onClick={this.deleteEntry} type="danger" icon="delete" />
                    )}
                </Button.Group>
            );
        }
        return (
            <Button
                onClick={() => this.startEdit(record)}
                icon={record.editing ? 'check' : 'edit'}
            />
        );
    };

    onBlurInput = (entity, value) => {
        const { isCreatingItem } = this.props;
        if (isCreatingItem) {
            this.props.editCreatingItem(entity, value);
        } else {
            this.props.editExistingItem(entity, value);
        }
    };

    renderInput = (text, record, entity) => {
        if (record.editing) {
            return (
                <Input
                    defaultValue={text}
                    placeholder={entity}
                    onBlur={e => this.onBlurInput(entity, e.target.value)}
                />
            );
        }
        return text;
    };

    doneEdit = record => {
        if (record.key === KEY_NEW) {
            this.props.createNewItem();
        } else {
            this.props.doneEditingItem();
        }
    };

    cancelEdit = () => {
        this.props.cancelEditItem();
    };

    deleteEntry = () => {
        this.props.deleteSelectedItem();
    };

    startEdit = record => {
        this.props.startEditingItem(record.domain, record.range, record.key);
    };

    startCreatingItem = () => {
        this.props.startCreatingItem();
    };

    renderProductTable = () => {
        const { colorMapping, isEditingItem, isCreatingItem, errors } = this.props;
        const editingDomain = isEditingItem ? isEditingItem.key : '';

        if (!colorMapping.length) return 'No data';

        const dataSource = colorMapping.map(p => ({
            domain: p.domain,
            range: p.range,
            key: p.key,
            editing: p.key === editingDomain,
            error: errors[p.key]
        }));

        if (isCreatingItem) {
            const { domain, range, key } = isCreatingItem;

            dataSource.splice(0, 0, {
                domain,
                range,
                key,
                editing: true,
                error: null
            });
        }

        return <Table dataSource={dataSource} columns={this.getProductColumns()} />;
    };

    render() {
        const { isCreatingItem, selectedDictionary, isCreatingDictionary } = this.props;

        return (
            <Card
                title={`Colormap of ${selectedDictionary}`}
                extra={
                    <Button
                        type="primary"
                        icon="plus"
                        disabled={!!isCreatingItem || isCreatingDictionary}
                        onClick={this.startCreatingItem}
                    >
                        New Row
                    </Button>
                }
            >
                {this.renderProductTable()}
            </Card>
        );
    }
}

const mapStateToProps = state => ({
    selectedDictionary: state.dictionaries.current,
    isCreatingDictionary: state.dictionaries.isCreating,
    colorMapping: state.maps.colorMapping,
    isCreatingItem: state.maps.isCreatingItem,
    isEditingItem: state.maps.isEditingItem,
    errors: state.maps.errors
});
const mapDispatchToProps = dispatch => ({
    startCreatingItem: () => dispatch(startCreatingItem()),
    editCreatingItem: (entity, text) => dispatch(editCreatingItem(entity, text)),
    editExistingItem: (entity, text) => dispatch(editExistingItem(entity, text)),
    startEditingItem: (domain, range, key) => dispatch(startEditingItem(domain, range, key)),

    doneEditingItem: () => dispatch(doneEditingItem()),
    createNewItem: () => dispatch(createNewItem()),
    cancelEditItem: () => dispatch(cancelEditItem()),
    deleteSelectedItem: () => dispatch(deleteSelectedItem())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductTable);
